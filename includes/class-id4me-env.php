<?php

/**
 * Class ID4me_Exceptions
 */
class ID4me_Env {

	/**
	 * Log errors in a global form like in WordPress
	 * (so that we can collect them and show them when the time has come)
	 *
	 * @param string $message
	 */
	protected function log_error( $message ) {
		global $id4me_errors;

		if ( ! is_array( $id4me_errors ) ) {
			$id4me_errors = array();
		}

		$id4me_errors[] = $message;
	}

	/**
	 * Generate HTML code to render the error messages
	 *
	 * @return string
	 */
	protected function show_errors() {
		global $id4me_errors;

		if ( ! empty( $id4me_errors ) ) {
			foreach ( $id4me_errors as $message ) {
				return '<div class="message id4me_error">' . $message . '</div>';
			}
		}

		return '';
	}

	/**
	 * Return row error messages
	 *
	 * @return array
	 */
	protected function get_errors() {
		global $id4me_errors;

		return $id4me_errors;
	}

	/**
	 * Retrieve application environment
	 * (can be externally given through ENV param, in Docker for ex.)
	 */
	protected function get_env() {

		$env = getenv( 'ID4ME_ENV' );
		$allowed_envs = array( 'dev', 'prod' );

		if ( empty( $env ) || ! in_array( $env, $allowed_envs ) ) {
			return 'prod';
		} else {
			return $env;
		}
	}
}
