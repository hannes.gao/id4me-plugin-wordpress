<?php

/**
 * Class ID4me_Login
 */
class ID4me_Login extends ID4me_Env {

	/**
	 * Render the extra ID4me login form
	 * @action login_footer
	 */
	public function login_form() {
		global $interim_login;

		if ( $this->is_login_page() ) {
			$login_html = '';

			// Build the URL switching back to the standard WP login
			$wp_redirect_to = ! empty( $_GET['redirect_to'] ) ? esc_url( $_GET['redirect_to'] ) : '';

			// Show the errors that our processes eventually thrown
			$login_html .= $this->show_errors();

			// Render ID4me login form
			$login_html .= '
				<form action="' . wp_login_url() . '" id="id4me-loginform" method="get">
					<input type="hidden"
							name="id4me_action"
							value="connect">

					<label for="id4me-identifier">
						' . esc_html__( 'Enter your identifier:', 'id4me' ) . '<br>
						<input type="text"
								name="id4me_identifier"
								id="id4me-identifier"
								class="id4me-identifier">
					</label>

					<button type="submit"
							name="id4me_login_submit"
							class="id4me-login-submit id4me-button button button-primary button-large">
							' . esc_html__( 'Log in with ID4me', 'id4me' ) . '
					</button>

					<p class="id4me-login-or"><span>' . esc_html__( 'Or', 'id4me' ) . '</span></p>
					<p class="id4me-login-switch">
						<a href="' . wp_login_url( $wp_redirect_to ) . '" id="id4me-login-switch">
							' . esc_html__( 'Log in with WordPress', 'id4me' ) . '
						</a>
					</p>
				</form>
			';

			if ( ! $interim_login ) {
				$login_html .= '
					<p id="backtoblog" class="id4me-backtoblog">
						<a href="' . esc_url( home_url( '/' ) )  .'">' . sprintf( _x( '&larr; Back to %s', 'site' ), get_bloginfo( 'title', 'display' ) ) . '</a>
					</p>
				';
			}
			echo $login_html;
		}
	}

	/**
	 * Render the link getting back to ID4me login form
	 * @action login_form
	 */
	public function login_link() {

		$id4me_login_url = add_query_arg( array(
			'id4me_action' => 'login',
		) );
		echo '
			<p class="id4me-login-link">
				<a href="' . esc_url( $id4me_login_url ) . '" id="id4me-login-link">
					' . esc_html__( 'Log in with ID4me', 'id4me' ) . '
				</a>
			</p>
		';
	}

	/**
	 * Insert an "id4me" extra body class to differentiate WP login / ID4me login
	 * @action login_body_class
	 *
	 * @param  array $classes
	 * @return array
	 */
	public function login_body_class( $classes ) {

		// Are we on the ID4me login page?
		if ( ! empty( $_GET['id4me_action'] ) ) {
			$classes[] = 'id4me';
		}
		return $classes;
	}

	/**
	 * Check if we are in the WP login page
	 *
	 * @return boolean
	 */
	public function is_login_page() {

		return ! isset( $_REQUEST['action'] )
				|| ( 'login' === $_REQUEST['action'] );
	}

	/**
	 * Add the alternative login CSS/JS scripts
	 * @action login_enqueue_scripts
	 */
	public function enqueue_scripts() {

		if ( $this->is_login_page() ) {

			// Additional CSS
			wp_enqueue_style( 'id4me-login', plugin_dir_url( __DIR__ ) . 'assets/css/login.css' );
		}
	}
}
