<?php

/**
 * Class ID4me_WP_User_Meta
 */
class ID4me_User {

	/**
	 * ID4me username/domain
	 *
	 * @var string
	 */
	private $identifier;

	/**
	 * Constructor.
	 *
	 * @param string $identifier
	 */
	public function __construct( $identifier ) {
		$this->identifier = $identifier;
	}

	/**
	 * Search the WP_User existing for the ID4me identifier
	 *
	 * @return WP_User
	 * @throws Exception
	 */
	public function get_wp_user() {

		if ( ! $this->get_identifier() ) {
			throw new Exception( 'No identifier provided to identify the WordPress user' );
		}

		// Search user with ID4me identifier metadata
		$query = new WP_User_Query( array(
			'meta_key'   => 'id4me_identifier',
			'meta_value' => $this->get_identifier(),
		) );
		$users = $query->get_results();

		// Just one user must be found
		if ( count( $users ) > 1 ) {
			throw new Exception( 'Multiple WordPress users found with the provided identifier' );
		}
		if ( count( $users ) < 1 ) {
			throw new Exception( 'No WordPress users found with the provided identifier' );
		}

		return reset( $users );
	}

	/**
	 * @return string
	 */
	public function get_identifier() {
		return $this->identifier;
	}

	/**
	 * @param string $identifier
	 */
	public function set_identifier( $identifier ) {
		$this->identifier = $identifier;
	}

	/**
	 * Add the ID4me extra identifier field in the User Profile
	 * @action show_user_profile
	 * @action edit_user_profile
	 *
	 * @param WP_User $user
	 */
	public static function create_profile_fields( $user ) {

		$id4me_identifier = get_user_meta( $user->ID, 'id4me_identifier', true );

		echo '
			<h3>' . __( 'ID4me', 'id4me' ) . '</h3>

			<table class="form-table">
				<tr>
					<th><label for="id4me-identifier">' . __( 'ID4me identifier', 'id4me' ) . '</label></th>
					<td>
						<input type="text"
						       name="id4me_identifier"
						       id="id4me-identifier"
						       value="' . esc_attr__( $id4me_identifier ). '"
						       class="regular-text" /><br />

						<p class="description" id="id4me-identifier-description">
							' . __( 'The identifier allows you to log in with ID4me; ex. <strong>domain.com</strong>', 'id4me' ). '
						</p>
					</td>
				</tr>
			</table>
		';
	}

	/**
	 * Save the ID4me identifier during Profile Update
	 * @action personal_options_update
	 * @action edit_user_profile_update
	 *
	 * @param int $user_id
	 */
	public static function save_profile_fields( $user_id ) {

		if ( current_user_can( 'edit_user', $user_id ) ) {
			update_user_meta( $user_id, 'id4me_identifier', sanitize_text_field( $_POST['id4me_identifier'] ) );
		}
	}
}
